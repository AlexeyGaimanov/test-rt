# -*- coding: utf-8 -*-
from django.db import models


class Currency(models.Model):
    currency_code = models.CharField(verbose_name='Валюта', max_length=3, unique=True, db_index=True, db_column='code')
    value_against_dollar = models.FloatField(verbose_name='Отношение к доллару', db_column='value')

    class Meta:
        db_table = 'currency'
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюты'
        ordering = ['currency_code']

    def __str__(self):
        return self.currency_code + '\t-----------------\t' + str(self.value_against_dollar)