"""test_RT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from ConverterApp.views import MainView, ConvertHtmlView


urlpatterns = [
    url(r'^$', MainView.as_view(), name='main_page'),
    url(r'^in/html/$', ConvertHtmlView.as_view(), name='convert_html'),
    url(r'^(?P<amount>[0-9]+[.0-9]*)/(?P<currency_code_1>[a-zA-Z]{3})/to/(?P<currency_code_2>[a-zA-Z]{3})/',
        include('ConverterApp.urls', namespace='convert_url')),
    url(r'^admin/', admin.site.urls),
]
