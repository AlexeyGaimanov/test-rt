# -*- coding: utf-8 -*-
from celery.task import periodic_task
from datetime import timedelta
from ConverterApp.models import Currency
import requests


requests.packages.urllib3.disable_warnings()
APP_ID = 'b0ddf5222b71407394bbe4f493a05439'


@periodic_task(run_every=timedelta(seconds=3600))
def update_currency_table():
    receive_data = requests.get('https://openexchangerates.org/api/latest.json?app_id='+APP_ID)
    print receive_data.status_code
    rates = receive_data.json()['rates']
    #перевожу словарь валют с их значениями
    #в список кортежей [(code, value), ...]
    list_of_instances = rates.items()
    for currency_item in list_of_instances:
        currency_code = currency_item[0]
        value_against_dollar = currency_item[1]
        try:
            instance = Currency.objects.get(currency_code=currency_code)
        except Currency.DoesNotExist:
            instance = Currency.objects.create(currency_code=currency_code, value_against_dollar=value_against_dollar)
        else:
            instance.value_against_dollar = value_against_dollar
        instance.save()

