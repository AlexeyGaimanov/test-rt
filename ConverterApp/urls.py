from django.conf.urls import url
from ConverterApp.views import ConvertUrlInHtmlView, convert_url_in_text, convert_url_in_json

urlpatterns = [
    url(r'^in/text/$', convert_url_in_text, name='convert_url_in_text'),
    url(r'^in/json/$', convert_url_in_json, name='convert_url_in_json'),
    url(r'^in/html/$', ConvertUrlInHtmlView.as_view(), name='convert_url_in_html'),
]