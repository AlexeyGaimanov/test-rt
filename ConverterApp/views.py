# -*- coding: utf-8 -*-
from django.http import Http404, HttpResponse, JsonResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.base import TemplateView
from ConverterApp.forms import ConvertForm
from ConverterApp.models import Currency
from django.core.cache import cache


CACHE_TIME = 1800


def get_convert_amount(GET_parameters):
    amount = float(GET_parameters['amount'])
    code_1 = GET_parameters['currency_code_1'].upper()
    code_2 = GET_parameters['currency_code_2'].upper()
    from_currency = cache.get(code_1)
    if from_currency == None:
        from_currency = Currency.objects.get(currency_code=code_1)
        cache.set(code_1, from_currency, CACHE_TIME)
    into_currency = cache.get(code_2)
    if into_currency == None:
        into_currency = Currency.objects.get(currency_code=code_2)
        cache.set(code_2, into_currency, CACHE_TIME)
    return into_currency.value_against_dollar/from_currency.value_against_dollar*amount


def convert_url_in_text(request, **kwargs):
    try:
        converted_amount = get_convert_amount(kwargs)
    except Currency.DoesNotExist:
        error = 'Error: currency does not exist'
    except ValueError:
        error = 'Value Error'
    else:
        return HttpResponse(str(converted_amount))
    if error:
        return HttpResponse(error)


def convert_url_in_json(request, **kwargs):
    try:
        converted_amount = get_convert_amount(kwargs)
    except Currency.DoesNotExist:
        error = 'Error: currency does not exist'
    except ValueError:
        error = 'Value Error'
    else:
        return JsonResponse({'success': True, 'result': converted_amount})
    if error:
        return JsonResponse({'success': False, 'error': error})


class ConvertUrlInHtmlView(TemplateView):
    template_name = 'convert_page.html'
    covert_form = None
    into_currency = None

    def get(self, request, *args, **kwargs):
        try:
            self.converted_amount = get_convert_amount(self.kwargs)
        except (MultiValueDictKeyError, Currency.DoesNotExist, ValueError, KeyError):
            raise Http404
        self.into_currency = self.kwargs['currency_code_2'].upper()
        self.convert_form = ConvertForm(initial={'currency_code_1': self.kwargs['currency_code_1'].upper(),
                                                 'currency_code_2': self.into_currency,
                                                 'amount': self.kwargs['amount']})
        return super(ConvertUrlInHtmlView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ConvertUrlInHtmlView, self).get_context_data(**kwargs)
        context['convert_form'] = self.convert_form
        context['converted_amount'] = self.converted_amount
        context['into_currency'] = self.into_currency
        return context


class ConvertHtmlView(ConvertUrlInHtmlView):
    def get(self, request, *args, **kwargs):
        try:
            self.converted_amount = get_convert_amount(request.GET)
        except (MultiValueDictKeyError, Currency.DoesNotExist, ValueError, KeyError):
            raise Http404
        self.into_currency = request.GET['currency_code_2']
        self.convert_form = ConvertForm(initial={'currency_code_1': request.GET['currency_code_1'].upper(),
                                                 'currency_code_2': self.into_currency,
                                                 'amount': request.GET['amount']})
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class MainView(TemplateView):
    template_name = 'main.html'
    covert_form = None

    def get(self, request, *args, **kwargs):
        self.convert_form = ConvertForm()
        return super(MainView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        context['convert_form'] = self.convert_form
        return context
