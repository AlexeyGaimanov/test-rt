# -*- coding: utf-8 -*-
from django import forms
from ConverterApp.models import Currency


currency_list = Currency.objects.all()
currency_codes = []
for currency in currency_list:
    currency_codes.append((currency.currency_code, currency.currency_code))


class ConvertForm(forms.Form):
    currency_code_1 = forms.ChoiceField(label='Валюта', choices=currency_codes)
    currency_code_2 = forms.ChoiceField(label='Конвертировать в', choices=currency_codes)
    amount = forms.FloatField(label='Cумма',initial='0', required=True, error_messages={'required': 'Введите сумму'})